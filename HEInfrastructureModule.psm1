﻿#region module classes
class HEAcctMgtType{
    [string]$Description
    [string]$Details
    [string]$Information
    [string]$Domain
    [string]$UserCell
    [string]$Type
    HEAcctMgtType([string]$Type,[string]$Domain){
        $this.Type()
        $this.Domain = $Domain
        $this.Initialize()
    }
    [void] Initialize(){
        switch($this.Type){
            'RESUSERBIZ' { 
                $this.Description = 'Domain Account Password Reset ('+ $this.Domain + ')'
                $this.Details = 'Below is your new password, please use the link in this email to reset your password'
                $this.Information = '<br><a href="https://selfservice.healthedge.biz/RDWeb/Pages/en-US/">Self Service Password Reset</a>'
                $this.UserCell = '' 
              }
            'RESUSERINT' { 
                $this.Description = 'Domain Account Password Reset ('+ $this.Domain + ')'
                $this.Details = 'Below is your new password, please reset to a permanent password'
                $this.Information = ''
                $this.UserCell = ''
              }
            'RESUSERCUST' { 
                $this.Description = 'Customer Domain Account Password Reset ('+ $this.Domain + ')'
                $this.Details = 'Below is your new password'
                $this.Information = ''
                $this.UserCell = ''
              }
            'RESCXT' { 
                $this.Description = 'CXT Password Reset'
                $this.Details = 'Below is your new password'
                $this.Information = ''
                $this.UserCell = ''
              }
            'ADDCXT' { 
                $this.Description = 'CXT New User'
                $this.Details = 'Below is your new username and password'
                $this.Information = ''
                $this.UserCell = '<tr valign="top"><td style="text-align:right; border-color : #000000 #000000 #000000 #000000; border-style: none;" width="20%">USER:</td><td style="border-color : #000000 #000000 #000000 #000000; border-style: none;">%USR%</td></tr>'
              }
            'ADDUSERBIZ' { 
                $this.Description = 'Domain Account New User (healthedge.biz)'
                $this.Details = 'Below is your new username and password, please use the link in this email to reset your password'
                $this.Information = '<br><a href="https://selfservice.healthedge.biz/RDWeb/Pages/en-US/">Self Service Password Reset</a>'
                $this.UserCell = '<tr valign="top"><td style="text-align:right; border-color : #000000 #000000 #000000 #000000; border-style: none;" width="20%">USER:</td><td style="border-color : #000000 #000000 #000000 #000000; border-style: none;">%USR%</td></tr>' 
              }
            'ADDUSERCOM' { 
                $this.Description = 'Domain Account New User ('+ $this.Domain + ')'
                $this.Details = 'Below is your new username and password, please reset your password'
                $this.Information = ''
                $this.UserCell = '<tr valign="top"><td style="text-align:right; border-color : #000000 #000000 #000000 #000000; border-style: none;" width="20%">USER:</td><td style="border-color : #000000 #000000 #000000 #000000; border-style: none;">%USR%</td></tr>'
              }
           'ADDUSERCUST' { 
                $this.Description = 'Customer Domain Account New User ('+ $this.Domain + ')'
                $this.Details = 'Below is your new username and password'
                $this.Information = ''
                $this.UserCell = '<tr valign="top"><td style="text-align:right; border-color : #000000 #000000 #000000 #000000; border-style: none;" width="20%">USER:</td><td style="border-color : #000000 #000000 #000000 #000000; border-style: none;">%USR%</td></tr>'
              }
            default { throw [Exception]::new("Unknown management type") }

        }
    }

}
class HEAcctMgtObject{
    [string]$Case
    
    [string]$Password
    [string]$FirstName
    [string]$LastName
    [string]$SamAccount
    [string]$Domain
    [string]$Subject
    [HEAcctMgtType]$Type
    [PSCredential]$Credential
    HEAcctMgtObject([string]$Identity,[int]$Type,[string]$Password,[string]$Domain,[string]$Case,[PSCredential]$Credential){
        $this.Type = [HEAcctMgtType]::new($Type, $Domain)
        $this.Credential = $Credential
        $ADAccount = Get-ADUser -Identity $Identity -Properties * -Server $Domain -Credential $Credential
        $this.Case = $Case
        $this.Password = $Password
        $this.FirstName = $ADAccount.GivenName
        $this.LastName = $ADAccount.Surname
        $this.Subject = "#secure SF Case: " + $this.Type.Description + " for " + $this.FirstName + " " + $this.LastName
    }

}
class HEMailMessage{
    [string]$Template
    [string]$Subject
    [string]$ToAddress
    [string]$FromAddress
    [hashtable]$Tokens
    [string]$Type
    [string]$LogoPath
    [string]$TPath
    [HEAcctMgtObject]$MailType
    [string]$LittleLogoPath
    [string]$Domain
    [string]$SelfServiceLink
    HEMailMessage([string]$Type){
        $this.Type = $Type.ToUpper()
        
        $this.LogoPath = $PSScriptRoot + "\EmailTemplates\HEMailMessageLogo.png"
        $this.LittleLogoPath = $PSScriptRoot + "\EmailTemplates\healthedge-logo-little.png"
        
        
        
        
        $TemplatePath = ''
        switch($this.Type){
           'RESET'{  $TemplatePath = $PSScriptRoot + "\EmailTemplates\HEResetMessageTemplate.html" }
           'ADD' {  $TemplatePath = $PSScriptRoot + "\EmailTemplates\HENewUserMessageTemplate.html"}
           'ACCESS' { }
           default { $TemplatePath = $PSScriptRoot + "\HEResetMessageTemplate.html"  }
        }
        try{
            $this.TPath = $TemplatePath
            $this.Template = [System.IO.File]::ReadAllText($TemplatePath)
        }catch{
            throw [Exception]::new("File not found.")
        }
    }
    
    [void] SetType([string]$Type){
        switch($Type.ToUpper()){
            'ADDUSERBIZ'{ $this.MailType = [HEAcctMgtObject]::new($type) }

        }
    }
    [void] SetDomain([string]$Domain) { 
        $this.Domain = $Domain 
        if($this.Domain.ToUpper().CompareTo('HEALTHEDGE.BIZ') -eq 0){
             $this.SelfServiceLink = '<p align=center><a align=center href="https://selfservice.healthedge.biz/RDWeb/Pages/en-US/">Self Service Password Reset</a><p>'
         }else { 
             $this.SelfServiceLink = ''
         }    
    }
    [string] GetBodyAsHtml([string]$Case,[string]$FirstName,[string]$Pass,[string]$OperationType,[string]$Identity){
       # Write-Host $Pass
        $date = $(Get-Date).ToShortDateString()
        switch($this.Type){
            'RESET' { 
                $this.Template = $this.Template.Replace("%CASE%",$Case)
                $this.Template = $this.Template.Replace("%FIRST%",$FirstName)
                $this.Template = $this.Template.Replace("%PWD%",$Pass)
                if($OperationType.ToUpper().CompareTo("INTERNAL") -eq 0){
                    $OperationType = $OperationType + ' (' + $this.Domain + ')'
                    
                }
                if($this.Domain.Length -ne 0){
                    $this.Template = $this.Template.Replace("%LINK%",$this.SelfServiceLink)
                }
                else{ $this.Template.Replace("%LINK%",'') }
                $this.Template = $this.Template.Replace("%PASSWORD_TYPE%",$OperationType)
                #$this.Template = $this.Template.Replace("%INFORMATION%",$Information)
                $this.Template = $this.Template.Replace("%DATE%",$date)
                break
                }
            'CREATE' {
                $this.Template = $this.Template.Replace("%CASE%",$Case)
                $this.Template = $this.Template.Replace("%FIRST%",$FirstName)
                $this.Template = $this.Template.Replace("%PWD%",$Pass)
                $this.Template = $this.Template.Replace("%SUBJECT%",$OperationType)
                $opType = $OperationType + " for the domain " + $this.Domain
                $this.Template = $this.Template.Replace("%OPERATION%",$opType)
                $this.Template = $this.Template.Replace("%DATE%",$date)
                $this.Template = $this.Template.Replace("%LINK%","")
                break
            
            }
        }
        return $this.Template
    }
    
}
class HELockoutEventQueue{
    [System.Collections.Generic.Dictionary[[string],[HELockoutUser]]]$EventQueue
    [PSCredential]$Credential
    [string]$Domain
    HELockoutEventQueue([string]$Domain,[PSCredential]$Credential){
        $this.EventQueue = [System.Collections.Generic.Dictionary[[string],[HELockoutUser]]]::new()
        $this.Domain = $Domain
        $this.Credential = $Credential
    }
    [void] Add([HELockoutEvent]$Event){
        if($this.EventQueue.Keys.Contains($Event.SamAccount)){
            $dupe = $false
            foreach($evt in $this.EventQueue[$Event.SamAccount].LockoutEvents){
                  
                  #Write-Host $Event.Compare($evt).ToString()
                  if($evt.Compare($Event)){
                    $dupe = $true
                    break
                  }
                  else{
                    $dupe = $false
                  }
            }
            if(-not $dupe){
               $this.EventQueue[$Event.SamAccount].Add($Event)   
            }
            
        }else{
            $this.EventQueue.Add($Event.SamAccount,[HELockoutUser]::new($Event.SamAccount,$this.Domain,$this.Credential))
            $this.EventQueue[$Event.SamAccount].Add($Event)
        }
    }
}
class HELockoutEvent{
    [string]$Computer
    [string]$SamAccount
    [datetime]$TimeStamp
    [string]$DomainController
    HELockoutEvent([string]$Computer,[string]$User,[datetime]$TimeStamp,[string]$DomainController){
        if($Computer.Contains('\\')) { $this.Computer = $Computer.Replace('\\','') }
        else { $this.Computer = $Computer }
        $this.SamAccount = $User
        $this.TimeStamp = $TimeStamp
        $this.DomainController = $DomainController
    }
    [bool] Compare([HELockoutEvent]$Event){
          $result = $false
          if($this.Computer.ToUpper().CompareTo($Event.Computer.ToUpper()) -eq 0){
            if($this.SamAccount.ToUpper().CompareTo($Event.SamAccount.ToUpper()) -eq 0){
                if($this.TimeStamp.CompareTo($Event.TimeStamp) -eq 0){
                    if($this.DomainController.ToUpper().CompareTo($Event.DomainController.ToUpper()) -eq 0){
                        $result = $true
                    }
                    else{ $result = $false }
                } else{ $result = $false }
            } 
          }else{
            $result = $false
          }
          return $result
    }
}
class HELockoutMonitor{
    [Microsoft.ActiveDirectory.Management.ADDomainController[]]$DomainControllers
    [HELockoutEvent[]]$LockoutEvents
    [Object[]]$Lockouts1 
    [PSCredential]$RunAs
    [string]$Domain
    [hashtable]$Filter
    [HELockoutEventQueue]$Queue
    HELockoutMonitor([string]$Domain,[PSCredential]$Credential){
        $this.RunAs = $Credential
        $this.Domain = $Domain
        
        $this.Init()
    }
    [void] Init(){
        $this.LockoutEvents = @()
        $this.Queue = [HELockoutEventQueue]::new($this.Domain,$this.RunAs)
        $this.DomainControllers = Get-ADDomainController -Filter * -Server $this.Domain -Credential $this.RunAs | Select-Object Hostname,Ipv4address,isglobalcatalog,site,forest,operatingsystem
    }
    [HELockoutEvent[]] GetLockouts([int]$Hours){
        $this.LockoutEvents = @()
        $lockouts = @()
        if($Hours -gt 0) { $Hours = -1 * $Hours }
        $this.Filter = @{ 
            LogName = 'Security'
            ID = 4740
            StartTime = [datetime]::Now.AddHours($Hours)
        }
        foreach($dc in $this.DomainControllers){
            [system.gc]::Collect()
            try{
              
              $lockouts += Invoke-Command -ScriptBlock { return $(Get-WinEvent -FilterHashtable $args[0]) } -ComputerName $dc.Hostname -ArgumentList $this.Filter -Credential $this.RunAs -ErrorAction SilentlyContinue
              
            }catch{
              continue
            }
        }
        $this.Lockouts1 = $lockouts
        foreach($lockout in $lockouts){
             $msg = $lockout.Message.Replace("`r","`n").REplace("`n`n","`n").Replace("`n`n","`n").Replace("A user account was locked out.`n","").Replace("Subject:`n","").Replace("Account That Was Locked Out:`n","").Replace("Additional Information:`n","")
             $elements = $msg.Split("`n")
             $user=""
             $computer=""
             foreach($element in $elements){
                $element = $element.Replace("`t","")
                $pieces = $element.Split(":")
                if(($pieces[0].ToUpper().CompareTo("ACCOUNT NAME") -eq 0) -and ($pieces[$pieces.Length] -ne '$')){
                    $user = $pieces[1]
                }elseif(($pieces[0].CompareTo("Caller Computer Name") -eq 0)){
                    $computer = $pieces[1]
                }

            }
            $tEvent = [HELockoutEvent]::new($computer,$user,$lockout.TimeCreated, $lockout.MachineName)
            $this.LockoutEvents += $tEvent
            $this.Queue.Add($tEvent)
            
        }
        
        return $this.LockoutEvents
    }
}
class HELockoutUser{
    [string]$SamAccount
    [Microsoft.ActiveDirectory.Management.ADUser]$Account
    [string]$Domain
    [HELockoutEvent[]]$LockoutEvents
    [PSCredential]$RunAs
    HELockoutUser([string]$SamAccount,[string]$Domain,[PSCredential]$Credential){
        $this.RunAs = $Credential
        $this.SamAccount = $SamAccount
        $this.Domain = $Domain
    }
    [void] Initialize(){
        $this.LockoutEvents = @()
        $this.Account = Get-ADUser -Identity $this.SamAccount -Server $this.Domain -Credential $this.RunAs -Properties *
    }
    [void] Add([HELockoutEvent]$Event){
        $this.LockoutEvents += $Event
    }
}
class HEUser{
    [string]$Identity
    [string]$OrganizationUnit
    [string]$EMail
    [string]$FirstName
    [string]$LastName
    [string]$Domain
    [System.Security.SecureString]$Password
    [PSCredential]$Credential
    [string[]]$MemberOf
    [bool]$CreateUser
    [Microsoft.ActiveDirectory.Management.ADUser]$TemplateUserAccount
    HEUser([string]$Id,[string]$First,[string]$Last,[string]$OU,[string]$Mail,[string]$Domain){
        $this.Domain = $domain
        $this.Identity = $Id
        $this.FirstName = $First
        $this.LastName = $Last
        $this.EMail = $mail
        $this.OrganizationUnit = $OU
        $this.CreateUser = $true
        
    }
    HEUser([string]$Id,[string]$First,[string]$Last,[string]$OU,[string]$Mail,[string]$Domain,[PSCredential]$Credential){
        $this.Domain = $domain
        $this.Identity = $Id
        $this.FirstName = $First
        $this.LastName = $Last
        $this.EMail = $mail
        $this.OrganizationUnit = $OU
        $this.Credential = $Credential
        $this.CreateUser = $true
        
    }
    HEUser([string]$Id,[string]$Domain,[PSCredential]$Credential){
        $account = Get-ADUser -Identity $Id -Server $Domain -Credential $Credential -Properties *
        $this.Identity = $Id
        $cn = "CN=" + $account.Name + ","
        $this.OrganizationUnit = $account.DistinguishedName.Replace($cn,'')
        $this.CreateUser = $false

    }
    [void] SetPassword([string]$Password){
        $this.Password = (ConvertTo-SecureString -AsPlainText "$Password" -Force)
    }
    [void] SetTemplate([string]$TemplateIdentity){
        if($this.Credential -eq $null){ 
            throw [Exception]::new("You are required to provide credentials for this operation")
        }
        try{
            $this.TemplateUserAccount = Get-ADUser -Server $this.Domain -Identity $TemplateIdentity -Properties * -Credential $this.Credential -ErrorAction SilentlyContinue
            $this.MemberOf = $this.TemplateUserAccount.MemberOf
        }catch{
            throw [Exception]::new("Template user does not exist")
        }
    }
    [void] SetCredential([PSCredential]$Credential){
        $this.Credential = $Credential
    }
    
}
class HEUserFunctions{
     [guid]$Guid
     HEUserFunctions(){
        
     }
     static [void] CreateUser([HEUser]$UserObj){
        $user = $null
        if($UserObj.Credential -eq $null) { throw [Exception]::new("Credentials cannot be NULL") }
        try{
            $user = Get-ADUser -Identity $UserObj.Identity -Server $UserObj.Domain -Credential $UserObj.Credential -ErrorAction SilentlyContinue
            
            throw [Exception]::new("User already exists!")
        }catch {
            continue
        }
        $FullName = $UserObj.FirstName + " " + $UserObj.LastName
        New-ADUser -Server $UserObj.Domain -Name $FullName -SamAccountName $UserObj.Identity -Surname $UserObj.LastName -GivenName $UserObj.FirstName -EmailAddress $UserObj.EMail -Path $UserObj.OrganizationUnit -Credential $UserObj.Credential -Confirm:$false
        foreach($group in $UserObj.MemberOf){
            Add-ADGroupMember -Identity $group -Members $UserObj.Identity -Server $UserObj.Domain -Credential $UserObj.Credential
        }
        
     }
     
     static [void] UpdateUser([HEUser]$User){}
     static [void] DisableUser([string]$Identity,[string]$domain,[PSCredential]$Credential){}
     static [void] ResetUserPassword([string]$Identity,[string]$Domain,[PSCredential]$Credential,$Password){
        
        Set-ADAccountPassword -Identity $Identity -Reset -NewPassword (ConvertTo-SecureString -AsPlainText "$Password" -Force) -Server $Domain -Credential $Credential
     }
     static [void] MoveUser([string]$Identity,[string]$Domain,[string]$OrgUnitDistinguishedName,[PSCredential]$Credential){
            $user = Get-ADUser -Identity $Identity -Server $Domain -Credential $Credential
            Move-ADObject -Identity $user.DistinguishedName -Server $Domain -Credential $Credential -TargetPath $OrgUnitDistinguishedName 
     }
}
class HEEventTypes{
    static [string] $PasswordReset = "HEPasswordReset"
    static [string] $AccountCreation = "HECreateAccoun"
    static [string] $DisableAccount = "HEDisableAccount"
    static [string] $EnableAccount = "HEEnableAccount"
    static [string] $DeleteAccount = "HEDeleteAccount"
    static [string] $ModifyAccount = "HEModifyAccount"
    
}
class HEServer{
    [string]$OctetOne
    [string]$OctetTwo
    [string]$OctetThree
    [string]$OctetFour
    [string]$Hostname
    [string]$ReverseZone
    [string]$ForwardZone
    [string]$OrganizationUnit
    [string]$NetworkId
    [string]$ResourcePtr
    [string]$FullyQuallifiedName
    [PSCredential]$Credential
    HEServer([string]$IPAddress,[string]$Hostname,[string]$Domain,[PSCredential]$Credential){
        $this.Credential = $Credential
        $this.Hostname = $Hostname
        $this.ForwardZone = $domain
        $this.FullyQuallifiedName = $Hostname + '.' + $Domain
        try{
        $parts = $IPAddress.Split('.')
        }catch{
            throw [Exception]::new("Invalid IPv4 IP Address")
        }
        if($parts.Count -ne 4) { throw [Exception]::new("Invalid IPv4 IP Address") }
        $this.OctetOne = $parts[0]
        $this.OctetTwo = $parts[1]
        $this.OctetThree = $parts[2]
        $this.OctetFour = $parts[3]
        $this.ReverseZone = $parts[2] + "." + $parts[1] + "." + $parts[0] + ".in-addr.arpa"
        $this.ResourcePtr = $parts[3] + "." + $parts[2] + "." + $parts[1] + "." + $parts[0] + ".in-addr.arpa"
        $this.NetworkId = $parts[0] + "." + $parts[1] + "." + $parts[2] + ".0/24"
    }
    [string] GetIpAddress(){
        $ip = $this.OctetOne + '.' + $this.OctetTwo + '.' + $this.OctetThree + '.' + $this.OctetFour
        return $ip
    }
    [void] CreatePtrRecord(){
        
        $zone = Get-DnsServerZone -Name $this.ReverseZone -ComputerName $this.GetDomain() -ErrorAction SilentlyContinue
          if($zone.Count -eq 0){
           Add-DnsServerPrimaryZone -NetworkID $this.NetworkId -ReplicationScope "Forest" -ComputerName $this.GetDomain() -DynamicUpdate NonsecureAndSecure
           }
            #Write-Host "Zone created"
        
        #$name = $this.OctetFour + '.' + $this.OctetThree + '.' + $this.OctetTwo + '.' + $this.OctetOne
        try{
            Add-DnsServerResourceRecordPtr -Name $this.ResourcePtr -ZoneName $this.ReverseZone  -ComputerName $this.GetDomain() -PtrDomainName $this.FullyQuallifiedName -ErrorAction SilentlyContinue
        }catch{
            return
        }
    }
    [string] GetDomain(){ return $this.ForwardZone }
}
class HECharacter{
    [int]$XPos
    [int]$YPos
    [char]$Character
    [System.ConsoleColor]$Foreground
    [System.ConsoleColor]$Background
    HECharacter([char]$Value,[int]$X,[int]$Y){
        $this.XPos = $X
        $this.YPos = $Y
        $this.Character = $Value
        $this.SetForeground([System.ConsoleColor]::White)
        $this.SetBackground([System.ConsoleColor]::Black)
    }
    [void] SetForeground([System.ConsoleColor]$Foreground){
        $this.Foreground = $Foreground
    }
    [void] SetBackground([System.ConsoleColor]$Background){
        $this.Background = $Background
    }
    [void] WriteCharacter(){
        [Console]::SetCursorPosition($this.XPos,$this.YPos)

        Write-Host $this.Character -ForegroundColor $this.Foreground -BackgroundColor $this.Background

    }
}
class HEConsole{
    [int]$Height
    [int]$Width
    [string]$Title
    [Char[,]]$Console
    [HECharacter[]]$ConsoleDetails
    [int]$Size
    HEConsole([int]$Height,[int]$Width,[int]$Title){
        $this.Height = $Height
        $this.Width = $Width
        $this.Title = $Title
        $this.Console = New-Object 'Char[,]' $Width, $Height
        
    }
    [void] Initialize(){
        $this.Size = $this.Width * $this.Height
        $this.Console = New-Object 'HECharacter[]' $this.Size
        [console]::WindowHeight = $this.Height
        [console]::WindowWidth = $this.Width
        
    }
    [void] Write([HECharacter]$Character){
        $this.Console[$Character.XPos,$Character.YPos] = $Character.Character

    }
    [void] Paint(){
        cls
        foreach($ConsoleDetail in $this.ConsoleDetails){
            $ConsoleDetail.WriteCharacter()
        }
    }
    [void] GetHECharacter([char]$Character,[int]$x,[int]$y){
        [HECharacter]$c = [HECharacter]::new($Character,$x, $y)
       
    
    }
    [void] WriteLine([string]$Line,[int]$XPos,[int]$YPos){
            $characters = $Line.ToCharArray()
            for($i=0;$i -lt $characters.Count;$i++){
                $ch = $this.GetHECharacter($characters[$i],$($XPos + $i),$YPos)
            }
    }
    [int] GetColorCode([string]$Color){
        $c = 0
        switch($Color.ToUpper()){
            'BLACK' { $c = 0}
            'BLUE' {  $c = 9 }
            'CYAN' { $c = 11 }
            'DARKBLUE' {  $c = 1 }
            'DARKGRAY' { $c = 8 }
            'DARKGREEN' { $c = 2 }
            'DARKMAGENTA' { $c = 5 }
            'DARKRED' { $c = 4 }
            'DARKYELLOW' { $c = 6 }
            'GRAY' { $c = 7 }
            'GREEN' { $c = 10 }
            'MAGENTA' { $c = 13 }
            'RED' { $c = 12 }
            'WHITE' { $c = 15 }
            'YELLOW' { $c = 14 }
            default { $c = $null }
        }
        return $c
    } 
    [void] Update(){
        
       
    }

}
class UserAcct{
    [string]$ComputerName 
    [string]$Username
    UserAcct([string]$u,[string]$c){
        $this.ComputerName = $c
        $this.Username = $u

    }
}
#endregion
#region module variables
New-Variable -Name HELogServer -Value "he1-admt-01.healthedge.biz" -Force -Scope Global -Description "Server that security event for adding/removing users, and password resets are writen" -Option ReadOnly
New-Variable -Name HEInfrastructureModulePath -Value $PSScriptRoot -Force -Scope Global -Description "Location of the HEInfrastructure Module" -Option ReadOnly
New-Variable -Name PSMailServer -Value "smtp.inf.healthedge.biz" -Force -Scope Global -Description "SMTP Server Address" -Option ReadOnly
New-Variable -Name HEDebug -Value $false -Force -Scope Global -Description "Set to true to enable debug output to standard out"
New-Variable -Name HEDomainData -Value $(Import-Csv $HEInfrastructureModulePath\Data\domain.data.csv) -Force -Scope Global -Description "Table of Healthedge client domains" -Option ReadOnly
#endregion
#region module functions
#region user lockout status and monitoring function
function Get-LockoutMonitor([Parameter(Mandatory=$true)][string]$Domain,[Parameter(Mandatory=$true)][PSCredential]$Credential){
    $LockoutMonitor = [HELockoutMonitor]::new($Domain,$Credential)
    return $LockoutMonitor
}
function Get-Lockouts([Parameter(Mandatory=$true)][string]$Domain,[Parameter(Mandatory=$true)][PSCredential]$Credential,[Parameter(Mandatory=$true)][int]$Hours){
    $mon = Get-LockoutMonitor -Domain $Domain -Credential $Credential
    $lockouts = $mon.GetLockouts($Hours)
    return $lockouts
}
function Get-LockoutEventQueue([Parameter(Mandatory=$true)][string]$Domain,[Parameter(Mandatory=$true)][PSCredential]$Credential,[Parameter(Mandatory=$true)][int]$Hours){
    $mon = Get-LockoutMonitor -Domain $Domain -Credential $Credential
    $lockouts = $mon.GetLockouts($Hours)
    return $lockouts
}
#endregion
#region user functions
function Add-UserAccount([string]$Identity,[string]$FirstName,[string]$LastName,[string]$Domain,[string]$OU,[string]$EmailAddress,[PSCredential]$Credential,[ValidateSet('CXT Password','Internal')]$Operation){
    switch($Operation){
        'Internal' { 
                 [HEUser]$user = [HEUser]::new($Identity,$FirstName,$LastName,$OU,$EmailAddress,$Domain,$Credential)
                 [HEUserFunctions]::CreateUser($user)
         }
        'CXT Password' { Write-Host "CXT Password" }

    }
}
function Get-LocalUser ($Computername = $env:COMPUTERNAME) {
    Get-WmiObject -Query "Select * from Win32_UserAccount Where LocalAccount = 'True'" -ComputerName $ComputerName |
    Select-Object -ExpandProperty Name 
}
function Get-LocalUserAccounts([string[]]$Computers){
$Users = @()
    foreach($Computer in $Computers){
        try{
            $tempUsers = Get-LocalUser -Computername $Computer
            foreach($tempUser in $tempUsers){
            $Users += [UserAcct]::new($tempUser,$Computer)
            }
        }catch{
            $date = (Get-Date).ToShortDateString().Replace('/','.')
            $logfile = "log-error-" + $date + ".log"
            "Error connecting to $Computer" | Add-Content -Path $logfile
        
        }
    }
    return $Users
}
function Reset-UserPasswords([string]$Identity,[PSCredential]$Credential,[string]$Password){
    if($Password -eq $null){
        $Password = [System.Web.Security.Membership]::GeneratePassword(16,3)
    }
    foreach($domain in $HEDomainData){
        [HEUserFunctions]::ResetUserPassword($Identity,$domain.Name,$Credential,$Password)
        $msg = "Set " + $Identity + " Password in " + $domain.Name
        
        Write-Host $msg
    }
    $msg = "Password: " + $Password

    Write-Host $msg
}
function Reset-UserPassword([string]$Identity,[string]$Domain,[string]$CaseNumber,[PSCredential]$Credential,[string]$Email=$null,[ValidateSet('CXT Password','Internal')]$Operation){
    $Password = [System.Web.Security.Membership]::GeneratePassword(20,3) 
    $Password = Set-Chars -InputString $Password
    if($env:USERNAME.StartsWith('a-')){
        $Whoami = Get-ADUser -Identity $env:USERNAME.Replace('a-','') -Properties * -Server headquarters.healthedge.com
    }else{
        $Whoami = Get-ADUser -Identity $env:USERNAME -Properties * -Server headquarters.healthedge.com
    }
    
    $Subject = "#secure Password Reset SF Case # " + $CaseNumber
    $message = [HEMailMessage]::new("RESET")
    $message.SetDomain($Domain)
    try{
        $user = Get-ADUser -Identity $Identity -Server $Domain -Credential $Credential -Properties * -ErrorAction SilentlyContinue
    }catch{
        $msg = "User not found in " + $Domain + " domain"
        Write-Host $msg -ForegroundColor Red
        return
    }
    $continue = $true
    if($user -eq $null){
        $msg = "User not found in " + $Domain + " domain"
        Write-Host $msg -ForegroundColor Red
        return

    }
    if((-not $user.Enabled) -and ($user -ne $null)){
        do{
        Write-Host "This account has been disabled. Enable account [Yes/No]: " -NoNewline
        $response = Read-Host
        switch($response.ToUpper()){
            'Y'{ 
                 Enable-ADAccount -Server $Domain -Identity $Identity -Credential $Credential 
                 $continue = $false
               }
            'YES'{
                   Enable-ADAccount -Server $Domain -Identity $Identity -Credential $Credential 
                   $continue = $false
                 }
            'N'{ return }
            'NO' { return }
            default { continue }
        }
        }while($continue)

    }
    if($Email.Length -eq 0){
        
        $Email = $user.EmailAddress
        
        if($Email.Length -eq 0){
            $continue = $true
            do{
            $msg =  "User " + $Identity + " in the domain " + $Domain + " currently does not have a email address" + "`nWould you like to set one now? [Yes/No]"
            Write-Host -NoNewline $msg
            $ans = Read-Host

            switch($ans.ToUpper()){
                'Y'{ 
                        do{
                            $msg = "Enter user's email address: "
                            Write-Host -NoNewline $msg
                            $Email = Read-Host
                            if($(Test-Email -Address $Email))
                                { 
                                  try{
                                    Set-ADUser -Identity $Identity -Server $domain -Credential $Credential -EmailAddress $Email -ErrorAction SilentlyContinue
                                    $innerContinue = $false 
                                  }catch{
                                    Write-Host "Unable to set e-mail address!" -ForegroundColor Red
                                  }
                                }
                            else{ $msg = "Invalid e-mail address try again."
                                  Write-Host $msg -ForegroundColor Red 
                                 }
                        }while($innerContinue)
                        $continue = $false
                   }
              'YES'{
                        $innerContinue = $true
                        do{
                            $msg = "Enter user's email address: "
                            Write-Host -NoNewline $msg
                            $Email = Read-Host
                            if($(Test-Email -Address $Email))
                                { 
                                  try{
                                    Set-ADUser -Identity $Identity -Server $domain -Credential $Credential -EmailAddress $Email -ErrorAction SilentlyContinue
                                    $innerContinue = $false 
                                  }catch{
                                    Write-Host "Unable to set e-mail address!" -ForegroundColor Red
                                  }
                                }
                            else{ $msg = "Invalid e-mail address try again."
                                  Write-Host $msg -ForegroundColor Red 
                                 }
                        }while($innerContinue)    
                        $continue = $false
                   }
                'N'{ return }
              'NO' { return }
           default { continue }
            }

            }while($continue)
         }   
        
        
    }
    elseif(($Email.Length -ne 0)){
       
        #Write-Host $user.EmailAddress $user.EmailAddress.Length -ne 0
        #Write-Host $Email
       if($user.EmailAddress.Length -eq 0){
            $nomatch = $true
       }
       elseif($Email.ToUpper().CompareTo($user.EmailAddress.ToUpper()) -eq 0){
            #Write-Host $user.EmailAddress
           # Write-Host $Email
            $nomatch = $false
       }else{
            #Write-Host $user.EmailAddress
            #Write-Host $email
            $nomatch = $true
       }
       #Write-Host $nomatch
       if($nomatch){
          $innerContinue = $true
          do{
            $msg = "The email address, " + $Email + " does not match the email associated with the account currently. [" + $user.EmailAddress + "]"
            Write-Host $msg -ForegroundColor Red
            $msg = "Would you like to set the accounts email to, " + $Email + ", now [Yes/No]?"
            Write-Host $msg -NoNewline -ForegroundColor Red
            $response = Read-Host
            switch($response.ToUpper()){
            'Y'{ 
                   try{
                       Set-ADUser -Server $Domain -Credential $Credential -Identity $Identity -EmailAddress $Email -ErrorAction SilentlyContinue
                        
                      }
                 catch{
                       $msg = "Unable to set user's email address to: " + $Email
                       Write-Host $msg -ForegroundColor Red
                       return
                  }
                  $innerContinue = $false 
                 
               }
            'YES'{
                   try{
                        Set-ADUser -Server $Domain -Credential $Credential -Identity $Identity -EmailAddress $Email -ErrorAction SilentlyContinue
                        
                      }
                   catch{
                        $msg = "Unable to set user's email address to: " + $Email
                        Write-Host $msg -ForegroundColor Red
                        return
                   }
                   $innerContinue = $false
                 }
            'N'{ return }
            'NO' { return }
            default { continue }
            }
          }While($innerContinue)
       }
       
    }
    
    [HEUserFunctions]::ResetUserPassword($Identity,$Domain,$Credential,$Password)
    
    $m = $message.GetBodyAsHtml($CaseNumber,$user.GivenName,$Password,$Operation,$user.SamAccountName)
    
    Set-ADUser -Identity $Identity -ChangePasswordAtLogon $false -Server $Domain -Credential $credential
    Send-MailMessage -Subject $subject -BodyAsHtml -Body $m -To $Email -From $Whoami.EmailAddress -SmtpServer $PSMailServer
    $exitmsg = "Password for " + $Identity + " has been reset"
    Write-Host $exitmsg -ForegroundColor Green
}
function Add-Groups([string[]]$Groups,[string[]]$Domains,[PSCredential]$Credential){
        foreach($Domain in $Domains){
            New-ADGroup -Name $Groups -Server $Domain -Credential $Credential -Path $(Get-OrganizationUnit -OrgUnit "HE-Users" -Domain $Domain -Credential $Credential).DistinguishedName
        }
}
function New-UserTermination([string]$Identity,[string]$Domain,[PSCredential]$Credentials,[string]$Case,[switch]$DisableAudit){
        $primary = 'CN=Domain Users,CN=Users'
        $stld = $Domain.Split('.')
        $terminated_ou = Get-OrgUnit -OrgUnit "Deprecated Objects" -Domain $Domain -Credential $Credentials
        if($null -eq $terminated_ou){
            New-OrgUnit -OrgUnit "Deprecated Objects" -Domain $Domain -Credential $Credentials
            $terminated_ou = Get-OrgUnit -OrgUnit "Deprecated Objects" -Domain $Domain -Credential $Credentials
        }
        for($i=0;$i -le $stld.Count;$i++){
            $primary += $(',DC=' + $stld[$i])
        } 
        $Description = "Terminated Account (SF #" + $Case + ")"
        try{
            $user = Get-ADUser -Identity $Identity -Server $Domain -Credential $Credentials -Properties * -ErrorAction SilentlyContinue
        }catch { 
            $msg = "User " + $Identity + " not found in the " + $Domain + " domain"
            Write-Host $msg -ForegroundColor Red
            return
        }
        foreach($group in $user.MemberOf){
            if($group.ToUpper().CompareTo($primary.ToUpper()) -eq 0){ continue }
            else { Remove-ADGroupMember -Identity $group -Members $user.DistinguishedName -Server cho.local -Credential $Credentials -Confirm:$false }
            $message = "Removed user " + $user.SamAccountName + " from " + $group + " AD group successfully"
            
            Write-Host $message -ForegroundColor Green
            $message = "Removed user " + $user.SamAccountName + " from " + $group + " AD group successfully"
            $emessage = $message + "`nCase: " + $Case
            #Write-HEEvent -Message $emessage -Source HEDisableAccount -Credential $Credentials -EventType Information -EventId 10
        }
        $message = "User " + $Identity + " disbled"
        Disable-ADAccount -Server $Domain -Identity $user.DistinguishedName -Credential $Credentials -Confirm:$false
        Move-ADObject -Server $Domain -Identity $user.DistinguishedName -TargetPath $terminated_ou.DistinguishedName -Credential $Credentials -Confirm:$false
        $emessage = $message + "`nCase: " + $Case
        #Write-HEEvent -Message $emessage -Source HEDisableAccount -Credential $Credentials -EventType Information -EventId 20
        Write-Host $message -ForegroundColor White

}
#endregion
#region utility function
function Set-Chars($InputString) {
    
    $chars = @('a','b','c','d','e','f','h','i','j','k','l','m','n','o','p','q','r','s','t','u','v','w','x','y','z')
    $chars += $chars.ToUpper()
    $SpecialChars = '[\{\[\(\)\]\}\/\`\\^]'
    $Replacement  = $($chars | Get-Random)

    $charArray = $InputString.ToCharArray()
    $ReplacementString = $null
    foreach($char in $charArray){
        if($char -match $SpecialChars){ $ReplacementString += $($chars | Get-Random).ToString() }
        else{ $ReplacementString += $char.ToString() }
    }
    return $ReplacementString
    
}

function Test-Email($Address){
     $address -match "^\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*$"
}
function Get-OrgUnit([string]$OrgUnit,[string]$Domain,[PSCredential]$Credential){
    try{
        $filter = 'Name -eq "'+ $OrgUnit+ '"' 
        $ou = Get-ADOrganizationalUnit -Filter $filter -Server $Domain -Credential $Credential -ErrorAction SilentlyContinue
    }catch{
        $msg = "Unable to locate " + $OrgUnit + " in " + $Domain
        Write-Host $msg -ForegroundColor Red
        return $null
    }
    
    return $ou
}
function New-OrgUnit([string]$OrgUnit,[string]$Domain,[PSCredential]$Credential){
    try{
        New-ADOrganizationalUnit -Name $OrgUnit -Server $Domain -Credential $Credential -Confirm:$false -ErrorAction SilentlyContinue
        
    }catch{
        $msg = "Unable to create OU " + $OrgUnit + " in " + $Domain
        Write-Host $msg -ForegroundColor Red
    }

}
function Write-HEEvent([string]$Message,[string]$Source,[PSCredential]$Credential,[string]$EventType,[int]$EventId){
    $source_user = $env:USERNAME
    $source_computer = hostname
    $source_details = "`nUser: " + $source_user + "`nComputer: " + $source_computer
    $Message += $source_details
    $arguments = @($Source,$Message,$EventType,$EventId)
    Invoke-Command -ComputerName $HELogServer -Credential $Credential -ScriptBlock { Write-EventLog -LogName HEAccountManagement -Source $args[0] -Message $args[1] -EntryType $args[2] -EventId $args[3] } -ArgumentList $arguments


}
function Get-LastLogonTime([string]$Identity,[string]$Domain,[PSCredential]$Credential){
    $user = Get-ADUser -Identity $Identity -Server $Domain -Credential $Credential -Properties *
    return $([datetime]::FromFileTime($user.lastLogon))
}
function Add-UserToSudoAcl([string[]]$User,[validateset('Payor','IWay','Connector')][string]$Application,[validateset('Production','NonProduction')][string]$Region,[string[]]$Domain,[switch]$Remove){
    $acl_iway = @{
        'Production' = 'ACL-IWAY-PROD'
        'NonProduction' = 'ACL-IWAY-NONPROD'

    }
    $acl_connector = @{
        'Production' = 'ACL-connector-PROD'
        'NonProduction' = 'ACL-connector-NONPROD'

    }
    $acl_payor = @{
        'Production' = 'ACL-payor-PROD'
        'NonProduction' = 'ACL-payor-NONPROD'

    }
    $acl_list = @{
        'IWay' = $acl_iway
        'Payor' = $acl_payor
        'Connector' = $acl_connector
    }
    foreach($d in $Domain){
        $group = $acl_list[$Application][$Region]
        foreach($u in $User){
            try{
                if($Remove){
                    Remove-ADGroupMember -Identity $group -Members $u -Server $d -ErrorAction SilentlyContinue -Confirm:$false
                    $color = "Green"
                    $msg = "Successfully removed $u to the group $group in the $d domain"
                }
                else{
                    Add-ADGroupMember -Identity $group -Members $u -Server $d -ErrorAction SilentlyContinue 
                    $color = "Green"
                    $msg = "Successfully added $u to the group $group in the $d domain"
                }
            }catch{
                if($Remove){
                    $color = "Red"
                    $msg = "Unable to remove $u to the group $group in the $d domain"
                }else{
                    $color = "Red"
                    $msg = "Unable to add $u to the group $group in the $d domain"
                }
            }
            Write-Host $msg -ForegroundColor $color
        }
    }

}
#endregion
#endregion
#region DNS management
function New-ReversePtrRecords([string]$Domain,[PSCredential]$Credential){
    # Creates PTR Records for all A Records in the specified -ZoneName.
# Uses a Class A Subnet for the reverse zone.
    $computerName = $Domain; 

# Get all the DNS A Records.
    $records = Get-DnsServerResourceRecord -ZoneName $Domain -RRType A -ComputerName $Domain;
     
    foreach ($record in $records) 
    { 
        
        if($record.Hostname.CompareTo('@') -eq 0){ continue }
        elseif($record.HostName.CompareTo('DomainDnsZones') -eq 0){ continue }
        elseif($record.HostName.CompareTo('ForestDnsZones') -eq 0){ continue }
        elseif($record.HostName.CompareTo('gc._msdcs') -eq 0) { continue }
        else{
            $server = [HEServer]::new($record.RecordData.IPv4Address.ToString(),$record.HostName,$Domain,$Credential)
            $server.CreatePtrRecord()
        }
    }



}
function Set-ReversRecords([string]$Domain,[PSCredential]$Credential){
    

}
#endregion